template '/etc/nginx/nginx.conf' do
	source "nginx.conf.erb"
	variables ({
		:web_server_name => node['gcs']['web_server_name']		
	})
end

template '/etc/gcs-app/application.conf' do
	source "application.conf.erb"
	variables ({
		:web_server_name => node['gcs']['web_server_name'],
		:pg_local_ip => node['gcs']['pg_local_ip']		
	})
end